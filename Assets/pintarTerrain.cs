﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pintarTerrain : MonoBehaviour
{

    [System.Serializable]
    public class SplatHeights
    {
        public int textureIndex;
        public int startHeight;
    }

    public SplatHeights[] splatHeights;


    void Start()
    {
        TerrainData terrainData = Terrain.activeTerrain.terrainData;
        float[,,] splatmapData = new float[terrainData.alphamapWidth,
                                            terrainData.alphamapHeight
                                            , terrainData.alphamapLayers];

        for (int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrainData.alphamapWidth; x++)
            {
                float terrainHeight = terrainData.GetHeight(y, x);

                float[] splat = new float[splatHeights.Length];

                for (int i = 0; i < splatHeights.Length; i++)
                {
                    if (i == splatHeights.Length-1 && 
                        terrainHeight >= splatHeights[i].startHeight)
                        splat[i] = 1;

                    else if (terrainHeight >= splatHeights[i].startHeight
                        && terrainHeight <= splatHeights[i+1].startHeight)
                        splat[i] = 1;
                }
                for (int j = 0; j < splatHeights.Length; j++)
                {
                    splatmapData[x, y, j] = splat[j];
                }

            }
        }
        terrainData.SetAlphamaps(0, 0, splatmapData);
    }  
  
}
